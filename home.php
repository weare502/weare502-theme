<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 502MEDIA
 */

get_header(); ?>
<?php
	if(get_field('header_background_image', intval( get_option('page_for_posts') ) ) ){
		echo "this one";
		$header_bg_url = get_field('header_background_image', intval( get_option('page_for_posts') ) );
	}
	elseif (has_post_thumbnail( intval( get_option('page_for_posts') ) ) ){
		echo "this two";
		$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id( intval( get_option('page_for_posts') ) ), 'theme_502media_feature_img');
		$header_bg_url = $thumb_feature[0];
	}
	else{
		$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
	}
	?>
<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
	<div class="row content-holder">
		<div class="col-sm-12 header-banner">
			<div class="header-banner-text">
				<h1><?php echo get_the_title( intval( get_option('page_for_posts') ) ); ?></h1>
			</div>
		</div>
	</div>
</section>

<div id="primary" class="content-area">
	<main id="main" class="site-main content-holder give-padding" role="main">

	<?php
	if ( have_posts() ) :

		/* Start the Loop */
		while ( have_posts() ) : the_post();

			/*
			 * Include the Post-Format-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
			 */
			get_template_part( 'template-parts/blog-single' );

		endwhile;

		the_posts_navigation( array( 'prev_text' => '<span class="fa fa-angle-double-left"></span> Older Posts', 'next_text' => 'Newer Posts <span class="fa fa-angle-double-right"></span>' ) );

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<?php
	if(get_field('footer_background_image', intval( get_option('page_for_posts') ) ) ){
		$footer_bg_url = get_field('footer_background_image', intval( get_option('page_for_posts') ));
	}
	else{
		$footer_bg_url = get_template_directory_uri() . '/images/footer-placeholder-img.png';
	}
	?>
	<section class="">
		<div class="row">
			<div class="col-sm-12 footer-banner" style="background-image: url('<?php echo $footer_bg_url; ?>');">
				<div class="footer-banner-text">
					<?php
					wp_reset_postdata();
					if(get_field('footer_title', intval( get_option('page_for_posts') ))){
						echo '<h1>' . get_field('footer_title', intval( get_option('page_for_posts') )) . '</h1>';
					}
					if(get_field('footer_subtitle', intval( get_option('page_for_posts') ))){
						echo '<h3><a class="button white" href="' . get_field('footer_link', intval( get_option('page_for_posts') )) . '">' . get_field('footer_subtitle', intval( get_option('page_for_posts') )) . '</a></h3>';
					}?>
				</div>
			</div>
		</div>
		<div class="row footer-edge">
			<div class="col-xs-6 social-media-links">
				<a href="https://www.facebook.com/502MediaGroup">
					<span class="fa-stack fa-1x">
						<i class="fa fa-circle-thin fa-stack-2x icon-background6"></i>
						<i class="fa fa-facebook fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://www.instagram.com/502mediagroup/">
					<span class="fa-stack fa-1x">
						<i class="fa fa-circle-thin fa-stack-2x icon-background6"></i>
						<i class="fa fa-instagram fa-stack-1x"></i>
					</span>
				</a>
				<a href="https://twitter.com/502mediagroup">
					<span class="fa-stack fa-1x">
						<i class="fa fa-circle-thin fa-stack-2x icon-background6"></i>
						<i class="fa fa-twitter fa-stack-1x"></i>
					</span>
				</a>
			</div>
			<div class="col-xs-6 get-in-touch">
				<a  href="/contact-us/"> <span>Let's Get In Touch </span> <i class="fa fa-comment-o" aria-hidden="true"></i> </a>
			</div>
		</div>
	</div>
</div>
</section>
</footer><!-- footer -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
	//Fix the position of toggle menu icon
	function fixed_header_with_adminBar() {
		var adminBarHeight	= jQuery('#wpadminbar').height();
		var menuToggleTop 	= 22;
		var topTotal 		= adminBarHeight + menuToggleTop;
		var containerHeight	= jQuery( window ).height();
		var pageHeight		= containerHeight - adminBarHeight;
		pageHeight = parseInt(pageHeight, 10);

		// jQuery('#mm-menu-toggle').css('top',topTotal);
		jQuery('#page').css('minHeight', pageHeight);

		console.log(pageHeight);
	}
	fixed_header_with_adminBar();
	jQuery( window ).resize(function() {
		fixed_header_with_adminBar();
	});
	</script>
</body>
</html>