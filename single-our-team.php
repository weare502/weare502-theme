<?php
/**
* Template Name: Single Team Member
*
* @package 502MEDIA
*/
get_header(); ?>
<?php
if(get_field('header_background_image')){
	$header_bg_url = get_field('header_background_image');
}
elseif (has_post_thumbnail()){
	$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
	$header_bg_url = $thumb_feature[0];
}
else{
	$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
}
?>
<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
	<div class="row content-holder">
		<div class="col-sm-12 header-banner">
			<div class="header-banner-text">
			</div>
		</div>
	</div>
</section><!-- .page-banner -->

<section class="container page-header content-holder">
	<div class="row">
		<div class="col-xs-12">
			<?php
				echo '<div class="text_score"><h1 class="fancy-heading">Meet ' . get_field('full_name') . '</h1><div class="u_score"></div> </div>';
				echo '<div class="para">' . get_field('meet_intro') . '</div>';
			?>
		</div>
	</div>
</section>

<div id="content" class="site-content full-width">
	<main id="main" class="site-main" role="main">

		<section class="container page-contents inset">
			<div class="row content-holder">
				<div class="col-xs-12 col-md-8">
					<?php while ( have_posts() ) : the_post(); ?>
						<h2 class="fancy-heading">Fun Facts / Biography</h2>
						<div class="fun-facts-bio">
							<?php the_field('fun_factsbio'); ?>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="col-xs-12 col-md-4">
					<img src="<?php echo get_field('bio_image')['url']; ?>" />
				</div>
			</div>
		</section><!-- .page-contents -->
			<?php $recent_posts = Timber::get_posts( array(
					'author'        =>  get_field('user_author')['ID'],
					'orderby'       =>  'post_date',
					'order'         =>  'ASC',
					'posts_per_page' => 2
				)
			); ?>

		<?php if ( ! empty( $recent_posts) ) : ?>
			<section class="container team-member-posts inset">
				<div class="row content-holder">
					<div class="col-xs-12">
						<div class="text_score">
							<h1 class="fancy-heading">Authored Blog Posts</h1>
							<div class="u_score"></div>
						</div>
					</div>
				</div>
				<div class="row content-holder">
					<?php the_field('blog_posts_paragraph'); ?>
				</div>
				<div class="row posts-wrapper">
					<?php foreach ( $recent_posts as $post ) : ?>
						<?php $image = new TimberImage($post->thumbnail); ?>
						<div class="col-xs-12 col-sm-6 team-member-post" style="background-image: url(<?php echo $image->src('large'); ?>);">
							<h2><a href='<?php echo $post->link(); ?>'><?php echo $post->title; ?></a></h2>
							<div class="tag-list"><a href="<?php echo $post->category()->link(); ?>"><?php echo $post->category()->title(); ?></a></div>
						</div>
					<?php endforeach ?>
				</div>
				<div class="row">
					<p class="center-text">
						<a href="/blog" class="button">View All Posts</a>
					</p>
				</div>
			</section>
		<?php endif; // Recent posts ?>
	</main><!-- #main -->
</div><!-- .container -->

<?php get_footer(); ?>