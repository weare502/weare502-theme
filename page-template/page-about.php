<?php
/**
 * Template Name: About Us
 *
 * @package 502MEDIA
 */
get_header(); ?>
<?php
		if(get_field('header_background_image')){
			$header_bg_url = get_field('header_background_image');
		}
		elseif (has_post_thumbnail()){
			$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
			$header_bg_url = $thumb_feature[0];
		}
		else{
			$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
		}
		?>
		<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
			<div class="row content-holder">
				<div class="col-sm-12 header-banner">
					<div class="header-banner-text">
						<?php
						if(get_field('header_title')){
							echo '<h1>' . get_field('header_title') . '</h1>';
						}
						else{
							the_title( '<h1>', '</h1>' );
						}
						if(get_field('header_subtitle')){
							echo '<h3>' . get_field('header_subtitle') . '</h3>';
						}
						?>
					</div>
				</div>
			</div>
		</section>
		<section class="container page-header content-holder">
			<div class="row">
				<div class="col-xs-12">
					<?php
						echo '<div class="text_score"><h1 class="fancy-heading">' . get_field('intro_title') . '</h1><div class="u_score"></div> </div>';
						echo '<div class="para">' . get_field('intro_paragraph') . '</div>';
					?>
				</div>
			</div>
		</section>
<div id="content" class="site-content full-width">
	<main id="main" class="site-main" role="main">

		<section class="container about-us-contents">
			<div class="row content-holder">
				<div class="col-sm-12">
				<div class="text_score"><h1 class="fancy-heading">Meet the Team</h1><div class="u_score"></div> </div>
					<div class="row" id="team-listing-isotope">
						<?php 
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$args = array(
							'post_type' => 'our-team',
							'posts_per_page' => 100,
							'paged' => $paged,
							'orderby' => 'title',
							'order' => 'ASC'
							);
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); 
						$terms = get_the_terms( $post->ID, 'category' );           
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$links = array();
						foreach ( $terms as $term ) {
							$links[] = $term->name;
						}
						$tax_links = join( " ", str_replace(' ', '-', $links));          
						$tax = strtolower($tax_links);
						else :  
							$tax = '';          
						endif; ?>

						<?php
						$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_team_member');
						$thumb_url	= $post_thumb[0];
						$post_url	= get_permalink();
						$content 	= get_the_content();
						?>
						<div class="all post-item col-xs-6 col-sm-4 <?php echo $tax; ?> ">
							<div class="thumbnail thumbnail-hover">
								<div class="img-holder">
									<img class="img-responsive" src="<?php echo $thumb_url; ?>" >
								</div>
								<a href="<?php echo $post_url ?>" title="<?php  the_title_attribute() ?>">
									<div class="entry">
										<h1><?php the_title() ?></h1>
										<?php if(get_field('header_subtitle'))
										{echo '<h5>' . get_field('header_subtitle') . '</h5>';}?>

									</div></a>
									<a href="<?php echo $post_url ?>" title="<?php  the_title_attribute() ?>" class="overlay"></a>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<?php if (function_exists("pagination")) {
								pagination($loop->max_num_pages);
							} ?>
						</div>
					</div>
				</div>
			</section><!-- .blog-contents -->

			<?php wp_reset_postdata(); ?>

			<section class="container page-contents inset">
				<div class="row content-holder">
					<div class="col-xs-12">
						<div class="text_score"><h1 class="fancy-heading">Our Space</h1><div class="u_score"></div> </div>
						<div>
							<?php the_field('our_office'); ?>
						</div>
					</div>
				</div>
				<div class="office-gallery">
					<?php foreach ( get_field('office_gallery') as $item ) : ?>
						<img src="<?php echo $item['sizes']['large']; ?>" />
					<?php endforeach; ?>
				</div>
			</section><!-- .page-contents -->

			<section class="container inset">
				<div class="row content-holder">
					<div class="col-xs-12">
						<div class="text_score"><h1 class="fancy-heading">Our Culture</h1><div class="u_score"></div> </div>
						<div>
							<?php the_field('our_culture'); ?>
						</div>
					</div>
				</div>
				<?php 
				$recent_posts = Timber::get_posts( array(
				    'category_name' => 'work-culture',
				    'orderby'       =>  'post_date',
				    'order'         =>  'ASC',
				    'posts_per_page' => 2
				    )
				); ?>
				<div class="row culture-posts clearfix">
					<?php foreach ( $recent_posts as $post ) : ?>
						<?php $image = new TimberImage($post->thumbnail); ?>
						<div class="col-xs-12 col-sm-6 culture-post" style="background-image: url(<?php echo $image->src('large'); ?>);">
							<h2><a href='<?php echo $post->link(); ?>'><?php echo $post->title; ?></a></h2>
							<div class="tag-list"><a href="<?php echo $post->category()->link(); ?>"><?php echo $post->category()->title(); ?></a></div>
						</div>
					<?php endforeach ?>
				</div>
				<p class="center-text">
					<a href="/blog" class="button">View All Posts</a>
				</p>
			</section>

		</main><!-- #main -->
	</div><!-- .container -->

	<?php get_footer(); ?>