<?php
/**
* Template Name: Case Study
*
* @package 502MEDIA
*/
get_header(); ?>
<?php
	if(get_field('header_background_image')){
		$header_bg_url = get_field('header_background_image');
	}
	elseif (has_post_thumbnail()){
		$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
		$header_bg_url = $thumb_feature[0];
	}
	else{
		$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
	}
	?>
	<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
		<div class="row content-holder">
			<div class="col-sm-12 header-banner">
				<div class="header-banner-text">
					<?php
					if(get_field('header_title')){
						echo '<h1>' . get_field('header_title') . '</h1>';
					}
					else{
						the_title( '<h1>', '</h1>' );
					}
					if(get_field('header_subtitle')){
						echo '<h3>' . get_field('header_subtitle') . '</h3>';
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="container page-header content-holder">
		<div class="row">
			<div class="col-xs-12">

			</div>
		</div>
	</section>
<div id="content" class="site-content full-width">
	<main id="main" class="site-main" role="main">
		<section class="container">
			<div class="text_score"><h1 class="fancy-heading">The Problem</h1><div class="u_score"></div> </div>
			<div class="content-holder">
				<?php the_field('problem'); ?>
			</div>
		</section>
		<section class="container featured-images-wrap inset">
			<div class="content-holder">
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_1')) )->src('large'); ?>);"></div>
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_2')) )->src('large'); ?>);"></div>
			</div>
		</section>
		<section class="container inset our-approach">
			<div class="text_score"><h1 class="fancy-heading">Our Approach</h1><div class="u_score"></div> </div>
			<div class="content-holder give-padding">
				<img src="<?php echo ( new TimberImage(get_field('approach_image')) )->src('large'); ?>" />
				<?php the_field('approach'); ?>
			</div>
		</section>
		<section class="case-quote">
			<blockquote>
				<p class="fancy-text"><?php the_field('quote') ?></p>
				<footer><?php the_field('quote_source'); ?></footer>
			</blockquote>
		</section>
		<section class="container featured-images-wrap overlap inset">
			<div class="content-holder">
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_3')) )->src('large'); ?>);"></div>
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_4')) )->src('large'); ?>);"></div>
				<p><?php the_field('featured_image_caption'); ?></p>
			</div>
		</section>
		<section class="container inset our-solution">
			<div class="text_score"><h1 class="fancy-heading">Our Solution</h1><div class="u_score"></div> </div>
			<div class="content-holder give-padding">
				<?php the_field('solution'); ?>
			</div>
		</section>
		<section class="case-quote">
			<blockquote>
				<p class="fancy-text"><?php the_field('quote_2') ?></p>
				<footer><?php the_field('quote_2_source'); ?></footer>
			</blockquote>
		</section>
		<section class="container inset results">
			<div class="text_score"><h1 class="fancy-heading">The Results</h1><div class="u_score"></div> </div>
			<div class="content-holder give-padding">
				<?php the_field('results'); ?>
			</div>
			<div class="give-padding numbered-results">
				<?php while ( have_rows('numbered_results') ) : the_row(); ?>
					<div class="result">
						<div class="number fancy-text"><?php the_sub_field('number'); ?></div>
						<div class="description"><?php the_sub_field('description'); ?></div>
					</div>
					<div class="divider"></div>
				<?php endwhile; ?>
			</div>
		</section>
		<section class="container featured-images-wrap inset">
			<div class="content-holder">
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_5')) )->src('large'); ?>);"></div>
				<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_field('featured_image_6')) )->src('large'); ?>);"></div>
			</div>
		</section>
		<section class="container inset likes">
			<p class="center-text">
				<button class="heart" data-likes="<?php the_field('likes'); ?>"><span class="fa fa-heart"></span></button>
			</p>
		</section>
	</main><!-- #main -->
</div><!-- .container -->

<?php get_footer(); ?>