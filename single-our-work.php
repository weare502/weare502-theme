<?php
/**
 * Template Name: Single Project
 *
 * @package 502MEDIA
 */
get_header(); ?>
<?php
	if(get_field('header_background_image')){
		$header_bg_url = get_field('header_background_image');
	}
	elseif (has_post_thumbnail()){
		$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
		$header_bg_url = $thumb_feature[0];
	}
	else{
		$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
	}
	?>
	<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
		<div class="row content-holder">
			<div class="col-sm-12 header-banner">
				<div class="header-banner-text">
					<?php
					if(get_field('header_title')){
						echo '<h1>' . get_field('header_title') . '</h1>';
					}
					else{
						the_title( '<h1>', '</h1>' );
					}
					if(get_field('header_subtitle')){
						echo '<h3>' . get_field('header_subtitle') . '</h3>';
					}
					?>
				</div>
			</div>
		</div>
	</section>

	<div id="content" class="site-content full-width">
	<main id="main" class="site-main" role="main">
		<section class="container">
			<div class="text_score"><h1 class="fancy-heading">About the Client</h1><div class="u_score"></div> </div>
			<div class="content-holder give-padding">
				<?php the_field('about'); ?>
			</div>
		</section>
		<div class="projects">
			<div class="wrapper">
				<?php while ( have_rows('projects') ) : the_row(); ?>
				<section class="container project inset">
					<div class="content-holder give-padding">
						<?php if ( get_sub_field('image') ) : ?>
							<div class="project-image <?php the_sub_field('image_alignment'); ?> ">
								<img src="<?php echo ( new TimberImage(get_sub_field('image')) )->src('large'); ?>" />
							</div>
						<?php endif; ?>
						<div class="project-description">
							<h2 class="fancy-heading"><?php the_sub_field('title'); ?></h2>
							<div><?php the_sub_field('description'); ?></div>
						</div>
					</div>
				</section>
				<?php endwhile; ?>
			</div>
		</div>
	</main><!-- #main -->
</div><!-- .container -->

<?php get_footer(); ?>