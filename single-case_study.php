<?php
/**
* Template Name: Case Study
*
* @package 502MEDIA
*/
get_header(); ?>
<?php
	if(get_field('header_background_image')){
		$header_bg_url = get_field('header_background_image');
	}
	elseif (has_post_thumbnail()){
		$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
		$header_bg_url = $thumb_feature[0];
	}
	else{
		$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
	}
	?>
	<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
		<div class="row content-holder">
			<div class="col-sm-12 header-banner">
				<div class="header-banner-text">
					<?php
					if(get_field('header_title')){
						echo '<h1>' . get_field('header_title') . '</h1>';
					}
					else{
						the_title( '<h1>', '</h1>' );
					}
					if(get_field('header_subtitle')){
						echo '<h3>' . get_field('header_subtitle') . '</h3>';
					}
					?>
				</div>
			</div>
		</div>
	</section>
	<section class="container page-header content-holder">
		<div class="row">
			<div class="col-xs-12">

			</div>
		</div>
	</section>
<div id="content" class="site-content full-width">
	<main id="main" class="site-main case-study-flex-sections" role="main">
		<?php while ( have_rows('flex_fields') ) : the_row(); ?>
			<?php if ( get_row_layout() == 'text_section') : ?>
				<section class="container <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<?php if ( get_sub_field('title') ) : ?>
						<div class="text_score"><h1 class="fancy-heading"><?php the_sub_field('title'); ?></h1><div class="u_score"></div></div>
					<?php endif; ?>
					<div class="content-holder">
						<?php the_sub_field('wysiwyg'); ?>
					</div>
				</section>
			<?php elseif ( get_row_layout() == 'quote') : ?>
				<section class="case-quote <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<blockquote>
						<p class="fancy-text"><?php the_sub_field('quote_text') ?></p>
						<footer><?php the_sub_field('quote_source'); ?></footer>
					</blockquote>
				</section>
			<?php elseif ( get_row_layout() == 'video') : ?>
				<section class="container case-video <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<div class="content-holder">
						<?php the_sub_field('video'); ?>
					</div>
				</section>
			<?php elseif ( get_row_layout() == 'photo_layout_1') : ?>
				<section class="container featured-images-wrap <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<div class="content-holder">
						<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_sub_field('first_image')) )->src('large'); ?>);"></div>
						<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_sub_field('second_image')) )->src('large'); ?>);"></div>
					</div>
				</section>
			<?php elseif ( get_row_layout() == 'photo_layout_2') : ?>
				<section class="container featured-images-wrap overlap <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<div class="content-holder">
						<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_sub_field('first_image')) )->src('large'); ?>);"></div>
						<div class="image" style="background-image: url(<?php echo ( new TimberImage(get_sub_field('second_image')) )->src('large'); ?>);"></div>
						<p><?php the_sub_field('image_caption'); ?></p>
					</div>
				</section>
			<?php elseif ( get_row_layout() == 'data_points') : ?>
				<section class="container results <?php echo get_sub_field('inset') ? 'inset' : ''; ?>">
					<div class="give-padding numbered-results">
						<?php while ( have_rows('data') ) : the_row(); ?>
							<div class="result">
							<?php $matches = array();
								preg_match_all('/\d/', get_sub_field('number'), $matches);
							?>
								<div class="number fancy-text">
									<!-- No spaces between span tags for visual purposes -->
									<span><?php the_sub_field('before_number'); ?></span><span class="inner-number" data-count="<?php echo implode('', $matches[0]); ?>">0</span><span><?php the_sub_field('after_number'); ?></span>
								</div>
								<div class="description"><?php the_sub_field('description'); ?></div>
							</div>
							<div class="divider"></div>
						<?php endwhile; ?>
					</div>
				</section>
			<?php endif; ?>
		<?php endwhile; ?>
		<section class="container inset likes">
			<p class="center-text">
				<button class="heart" data-likes="<?php the_field('likes'); ?>"><span class="fa fa-heart"></span></button>
			</p>
		</section>
	</main><!-- #main -->
</div><!-- .container -->

<?php get_footer(); ?>