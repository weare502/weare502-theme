<?php

use Timber\ImageHelper;

/**
 * Template Name: Case Study Archive
 *
 * @package 502MEDIA
 */
get_header(); ?>
<?php
	if(get_field('header_background_image')){
		$header_bg_url = get_field('header_background_image');
	}
	elseif (has_post_thumbnail()){
		$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
		$header_bg_url = $thumb_feature[0];
	}
	else{
		$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
	}
	?>
	<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
		<div class="row content-holder">
			<div class="col-sm-12 header-banner">
				<div class="header-banner-text">
					<h1>Our Work</h1>
					<h3>See Stories in Action</h3>
				</div>
			</div>
		</div>
	</section>

<div id="content" class="site-content full-width">
	<main id="main" class="site-main" role="main">

		<section class="container ours-work-contents">
			<div class="row content-holder">
				<div class="col-sm-12">
					<div class="row" id="post-listing-isotope">

						<?php 
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						$args = array(
							'post_type' => 'case_study',
							'posts_per_page' => 100,
							'paged' => $paged
							);
						$loop = new WP_Query( $args );
						$i = 0;
						while ( $loop->have_posts() ) : $loop->the_post(); 
						$i++;
						$terms = get_the_terms( $post->ID, 'category' );           
						if ( $terms && ! is_wp_error( $terms ) ) : 
							$links = array();
						foreach ( $terms as $term ) {
							$links[] = $term->name;
						}
						$tax_links = join( " ", str_replace(' ', '-', $links));          
						$tax = strtolower($tax_links);
						else :  
							$tax = '';          
						endif; ?>

						<?php
						if(get_field('grid_size')){

							if(get_field('grid_size') == 11){
								$col_size = "col-xs-12 col-sm-6 col-md-8 grid_size_22";
								$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_work_sm');
							}
							elseif (get_field('grid_size') == 12){
								$col_size = "col-xs-12 col-sm-6 col-md-6 grid_size_12";
								$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_work_mid');
							}
							else{
								$col_size = "col-xs-12 col-sm-6 col-md-8 grid_size_22";
								$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_work_big');
							}
						} else{
							$col_size = "col-xs-12 col-sm-6 grid_size_12";
							$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_work_sm');
						}

						if ( $i === 1 ){
							$col_size = "col-xs-12 col-sm-6 col-md-8 grid_size_22";
							$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_work_big');
						}
						if ($i === 2 || $i === 3 ){
							$col_size = "col-xs-12 col-sm-6 col-md-4 grid_size_11";
						}

						$thumb_url	= $post_thumb[0];
						$post_url	= get_permalink();
						$content 	= get_the_content();
						?>

						<div class="all post-item <?php echo $col_size; echo $tax; ?> ">
							<div class="thumbnail thumbnail-hover">
								<div class="img-holder">
									<?php $image = ( new TimberImage( get_post_thumbnail_id() ) )->src(); ?>
									<img class="img-responsive" src="<?php echo Timber\ImageHelper::resize( $image, 900, 600 ); ?>" >
								</div>
								<div class="entry">
									<h3><?php the_title() ?></h3>
									<h5>Featured Case Study</h5>
									
								</div>
								<a href="<?php echo $post_url ?>" title="<?php  the_title_attribute() ?>" class="overlay"></a>
							</div>
						</div>
					<?php endwhile; ?>

					<?php
							$args = array(
								'post_type' => 'our-work',
								'posts_per_page' => 100,
								'paged' => $paged
								);
							$loop = new WP_Query( $args );
							$i = 0;
							while ( $loop->have_posts() ) : $loop->the_post(); 
							$i++;
							$terms = get_the_terms( $post->ID, 'category' );           
							if ( $terms && ! is_wp_error( $terms ) ) : 
								$links = array();
							foreach ( $terms as $term ) {
								$links[] = $term->name;
							}
							$tax_links = join( " ", str_replace(' ', '-', $links));          
							$tax = strtolower($tax_links);
							else :  
								$tax = '';          
							endif; 
							$col_size = "col-xs-12 col-sm-6 grid_size_12";
							$post_url	= get_permalink();
							$content 	= get_the_content();
						?>

							<div class="all post-item <?php echo $col_size; echo $tax; ?> ">
								<div class="thumbnail thumbnail-hover">
									<div class="img-holder">
										<?php $image = ( new TimberImage( get_post_thumbnail_id() ) )->src(); ?>
										<img class="img-responsive" src="<?php echo Timber\ImageHelper::resize( $image, 900, 600 ); ?>" >
									</div>
									<div class="entry">
										<h3><?php the_title() ?></h3>
										
									</div>
									<a href="<?php echo $post_url ?>" title="<?php  the_title_attribute() ?>" class="overlay"></a>
								</div>
							</div>
						<?php endwhile; ?>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php if (function_exists("pagination")) {
							pagination($loop->max_num_pages);
						} ?>
					</div>
				</div>
			</div>
		</section><!-- .blog-contents -->

		<section class="container page-contents">
			<div class="row content-holder">
				<div class="col-xs-12">
					<?php while ( have_posts() ) : the_post(); 
					the_content(); 
					endwhile; ?>
				</div>
			</div>
		</section><!-- .page-contents -->

	</main><!-- #main -->
</div><!-- .container -->
<script type="text/javascript">
jQuery(document).ready(function(){
	var workItems = jQuery('#post-listing-isotope .post-item');
	var i = 0;
    var animateWorkItems = function () {
        jQuery(workItems[i++]).addClass('fadeInUp').css('visibility', 'visible');
        if (i < workItems.length) {
        	setTimeout(animateWorkItems, 200);
        }
    };
    animateWorkItems();
});
</script>
<?php get_footer(); ?>