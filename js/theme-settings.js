(function($) {

    /**
     * FitVids JS
     */
    ;(function( $ ){

        'use strict';

        $.fn.fitVids = function( options ) {
            var settings = {
                customSelector: null,
                ignore: null
            };

            if(!document.getElementById('fit-vids-style')) {
                // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
                var head = document.head || document.getElementsByTagName('head')[0];
                var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
                var div = document.createElement("div");
                div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
                head.appendChild(div.childNodes[1]);
            }

            if ( options ) {
                $.extend( settings, options );
            }

            return this.each(function(){
                var selectors = [
                    'iframe[src*="player.vimeo.com"]',
                    'iframe[src*="youtube.com"]',
                    'iframe[src*="youtube-nocookie.com"]',
                    'iframe[src*="kickstarter.com"][src*="video.html"]',
                    'object',
                    'embed'
                ];

                if (settings.customSelector) {
                    selectors.push(settings.customSelector);
                }

                var ignoreList = '.fitvidsignore';

                if(settings.ignore) {
                    ignoreList = ignoreList + ', ' + settings.ignore;
                }

                var $allVideos = $(this).find(selectors.join(','));
                $allVideos = $allVideos.not('object object'); // SwfObj conflict patch
                $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

                $allVideos.each(function(){
                    var $this = $(this);
                    if($this.parents(ignoreList).length > 0) {
                        return; // Disable FitVids on this video.
                    }
                    if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
                    if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
                    {
                        $this.attr('height', 9);
                        $this.attr('width', 16);
                    }
                    var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
                            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
                            aspectRatio = height / width;
                    if(!$this.attr('name')){
                        var videoName = 'fitvid' + $.fn.fitVids._count;
                        $this.attr('name', videoName);
                        $.fn.fitVids._count++;
                    }
                    $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
                    $this.removeAttr('height').removeAttr('width');
                });
            });
        };
        
        // Internal counter for unique video names.
        $.fn.fitVids._count = 0;
        
    // Works with either jQuery or Zepto
    })( window.jQuery || window.Zepto );

    /***
    Primary Nav manu Toggle hide/show
    ***/
    jQuery("#hamburger").click(function() {
        jQuery("#site-nav").fadeToggle("slow");
    });

    /***
    Menu Button Animation
    ***/
    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    };

    function toggleHandler(toggle) {
        toggle.addEventListener("click", function(e) {
            e.preventDefault();
            (this.classList.contains("is-active") === true) ? this.classList.remove("is-active"): this.classList.add("is-active");
        });
    };


    /******************************
     Sidebar Push Slide
     ******************************/
     function sideHeaderInit() {
        var sideheader = jQuery('#sideheader');
        if (sideheader.length) {
            sideheader.perfectScrollbar();
            //sideheader.hide();
            jQuery('#hamburger').on('click', function(event) {
                event.preventDefault();
                event.stopPropagation();

                if ($(sideheader).is('.is-visible')) {
                    $(sideheader).removeClass('is-visible');
                    
                    // $('#hamburger').addClass("is-active");
                    sideheader.hide(900);
                } else {
                    $(sideheader).addClass('is-visible');
                    
                    sideheader.show();
                }


                jQuery('body').toggleClass('sideheader-visible');
            });

            jQuery(document).on('click', function(e) {

                var _element = sideheader;

                if (!_element.is(e.target) // if the target of the click isn't the container...
                    &&
                    _element.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    sideheader.removeClass('is-visible');
                    $('#hamburger').removeClass("is-active");
                    sideheader.hide(900);
                    jQuery('body').removeClass('sideheader-visible');
                }
            });

            jQuery(window).on('scroll', function(e) {
                sideheader.removeClass('is-visible');
                $('#hamburger').removeClass("is-active");
                sideheader.hide(900);
                jQuery('body').removeClass('sideheader-visible');
            });
        };
    }
    /* Calls Sideheader Init script  */
    sideHeaderInit();


    /******************************
     Library: owl.carousel
     ******************************/

    //  $("#company-logo-slider").owlCarousel({
    //  	items : 4,
    //  	navigation : true,
    //  	itemsTablet: [768,4],
    //  	itemsTabletSmall: [600,3],
    //  	itemsMobile : [400,2],
    //  	singleItem : false,
    //  	itemsScaleUp : false,
    //  	navigationText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    //  });

    // //
    // document.addEventListener("touchstart", function(){}, true);



    /******************************
     Library: Isotope
     ******************************/

     if ($.isFunction($.fn.imagesLoaded) ) {

     	var container	= '#post-listing-isotope';

     	// $(container).imagesLoaded( function() {
      //      $(container).isotope({
      //           layoutMode: 'packery',
      //           packery: {
      //               gutter: 1
      //           },
      //       });
      //   });
     };

    /******************************
     Library: perfect-scrollbar
     ******************************/
    // $('#mm-menu').perfectScrollbar();


    /******************
    Backgroud Gradient - https://codepen.io/quasimondo/pen/lDdrF
    ******************/
    $('body.page-template-page-home #page').addClass('gradient-bg');
    $('body.archive.category .page-banner').addClass('gradient-bg');
    $('body.archive.category .footer-banner').addClass('gradient-bg');

    var colors = new Array(
        [230,80,45], [17,61,179], [230,80,45], [167,234,69], [230,80,45], [221,154,65]);
    colors = Array([230,80,45],[221,154,65],[230,80,45],[221,154,65]);

    var step = 0;
    //color table indices for: 
    // current color left
    // next color left
    // current color right
    // next color right
    var colorIndices = [0, 1, 2, 3];

    //transition speed
    var gradientSpeed = 0.002;

    function updateGradient() {

        if ($ === undefined) return;

        var c0_0 = colors[colorIndices[0]];
        var c0_1 = colors[colorIndices[1]];
        var c1_0 = colors[colorIndices[2]];
        var c1_1 = colors[colorIndices[3]];

        var istep = 1 - step;
        var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
        var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
        var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
        var color1 = "rgb(" + r1 + "," + g1 + "," + b1 + ")";
        // var color1 = "rgba(" + r1 + "," + g1 + "," + b1 + ", .5)";

        var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
        var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
        var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
        var color2 = "rgb(" + r2 + "," + g2 + "," + b2 + ")";
        // var color2 = "rgba(" + r2 + "," + g2 + "," + b2 + ", .5)";

        $('.gradient-bg').css({
            background: "-webkit-gradient(linear, left top, right top, from(" + color1 + "), to(" + color2 + "))"
        }).css({
            background: "-moz-linear-gradient(left, " + color1 + " 0%, " + color2 + " 100%)"
        });

        step += gradientSpeed;
        if (step >= 1) {
            step %= 1;
            colorIndices[0] = colorIndices[1];
            colorIndices[2] = colorIndices[3];

            //pick two new target color indices
            //do not pick the same as the current one
            colorIndices[1] = (colorIndices[1] + Math.floor(1 + Math.random() * (colors.length - 1))) % colors.length;
            colorIndices[3] = (colorIndices[3] + Math.floor(1 + Math.random() * (colors.length - 1))) % colors.length;

        }
    }

    setInterval(updateGradient, 10);


    /******************************
	 Other settings
	 ******************************/

    $(".remove-link a").removeAttr("href");

    if ($.isFunction($.fn.imagesLoaded)) {

        $('#post-listing-isotope .post-item .grid_size_11').matchHeight();
        // $('#post-listing-isotope .post-item .grid_size_12').matchHeight();
        // $('#post-listing-isotope .post-item .grid_size_22').matchHeight();

        function equal_size() {
            var entryHeight = $('.page-template-page-blog #post-listing-isotope .post-item .entry').height();
            var totalHeight = entryHeight + 50;
            $('body.page-template-page-blog #post-listing-isotope .post-item .thumbnail').css('height', totalHeight);
        }

        // equal_size();

        $(window).resize(function() {
            // equal_size();
        });
    };

    $('.heart').click(function(){
        $(this).attr('data-likes', parseInt( $(this).attr('data-likes') ) + 1 );
        $(this).addClass('clicked');
        $(this).attr('disabled', true);
    });

    // home_banner_adjustment

    function home_banner_adjustment() {
        var headerHeight = 0;
        var bannerHeight = 0;
        var bannerHalfHeight = 0;
        var containerHeight = 0;
        var bannerPosition = 0;
        var bannerTop = 0;
        var bannerTopMargin = 0;

        headerHeight = $('#masthead').height();
        bannerHeight = $('.banner-text').height();
        containerHeight = $(window).height();

        bannerHalfHeight = bannerHeight / 2;
        bannerPosition = (containerHeight / 2) - bannerHalfHeight;
        bannerTop = bannerPosition; //bannerPosition - headerHeight;
        bannerTopMargin = parseInt(bannerTop, 10);

        $('.banner-text').css("marginTop", bannerTopMargin);

        // console.log(bannerHeight);
        // console.log(bannerPosition);

    }

    $(window).load(function() {
        home_banner_adjustment();
        $('.banner-text h1').css("visibility", "visible").addClass('fade-in');
        // grid_adjustment();
    });

    $(window).resize(function() {
        home_banner_adjustment();
        // grid_adjustment();
    });

    $('body').fitVids();

    window.sr = ScrollReveal();
    sr.reveal('.case-study-flex-sections > section', {
        scale: 1,
        viewFactor: .3
    });
    sr.reveal('.projects .project', {
        scale: 1,
        viewFactor: .3,
        afterReveal: function(el){
            $(el).find('.project-image.left').addClass('slideInLeft');
            $(el).find('.project-image.right').addClass('slideInRight');
        }
    });
    sr.reveal('.case-quote', {
        duration: 10,
        viewFactor: .9,
        scale: 1,
        afterReveal: function(el){
            $(el).find('p').addClass('slideInLeft');
            $(el).find('footer').addClass('slideInRight');
        }
    });
    sr.reveal('.case-study-flex-sections .results', {
        scale: 1,
        viewFactor: .5,
        afterReveal: function(el){
            // https://codepen.io/hi-im-si/pen/uhxFn
            $(el).find('.inner-number').each(function() {
              var $this = $(this),
                  countTo = $this.attr('data-count');
                  $this.siblings().addClass('slideInUp');
              
              $({ countNum: $this.text()}).animate({
                countNum: countTo
              },
              {
                duration: 2000,
                easing:'linear',
                step: function() {
                  $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                  $this.text(this.countNum);
                }
              });  
            });
        }
    });
    sr.reveal('.page-template-page-about #main > section', {
        scale: 1,
        viewFactor: .3,
        afterReveal: function(el){
            var items = $(el).find('.post-item');
            var i = 0;
            var animateItems = function () {
                jQuery(items[i++]).addClass('fadeInUp').css('visibility', 'visible');
                if (i < items.length) {
                    setTimeout(animateItems, 200);
                }
            };
            animateItems();
        }
    });
    sr.reveal('.single-our-team #main > section', {
        scale: 1,
        viewFactor: .3,
        afterReveal: function(el){
            $(el).find('img').addClass('slideInRight');
            // $(el).find('footer').addClass('slideInRight');
        }
    });
    sr.reveal('.office-gallery img', {
        scale: 1,
        viewFactor: 1,
    });
    // sr.reveal('.bar');


})(jQuery);