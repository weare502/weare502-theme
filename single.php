<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package 502MEDIA
 */


get_header(); ?>
<?php
if(get_field('header_background_image')){
	$header_bg_url = get_field('header_background_image');
}
elseif (has_post_thumbnail()){
	$thumb_feature = wp_get_attachment_image_src( get_post_thumbnail_id(), 'theme_502media_feature_img');
	$header_bg_url = $thumb_feature[0];
}
else{
	$header_bg_url = get_template_directory_uri() . '/images/header-placeholder-img.png';
}
?>
<section class="container page-banner" style="background-image: url('<?php echo $header_bg_url; ?>');">
	<div class="row content-holder">
		<div class="col-sm-12 header-banner">
			<div class="header-banner-text">
				<?php
				if(get_field('header_title')){
					echo '<h1>' . get_field('header_title') . '</h1>';
				}
				else{
					the_title( '<h1>', '</h1>' );
				}
				if(get_field('header_subtitle')){
					echo '<h3>' . get_field('header_subtitle') . '</h3>';
				}
				?>
			</div>
		</div>
	</div>
</section>

<div id="primary" class="site-content full-width">
	<main id="main" class="site-main" role="main">
		<section class="container single-post-contents">
			<div class="row content-holder">
				<div class="col-xs-12">
					<?php while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', get_post_format() );
					endwhile; // End of the loop. ?>
				</div>
			</div>
		</section><!-- .single-post-contents -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
