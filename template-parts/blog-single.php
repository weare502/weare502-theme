<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 502MEDIA
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		<?php if ( 'post' === get_post_type() ) : ?>
			<!-- <div class="entry-meta">
				<?php theme_502media_posted_on(); ?>
			</div> --><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

<!-- 	<footer class="entry-footer">
		<?php theme_502media_entry_footer(); ?>
	</footer> -->
</article><!-- #post-## -->
